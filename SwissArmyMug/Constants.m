
#import "Constants.h"

NSString* const OAUTH_SCOPE = @"https://www.googleapis.com/auth/books";
NSString* const OAUTH_AUTH_URL = @"https://accounts.google.com/o/oauth2/auth";
NSString* const OAUTH_TOKEN_URL = @"https://accounts.google.com/o/oauth2/token";
// There are two available options for the redirect URL. 'urn:ietf:wg:oauth:2.0:oob' returns the authorization code in the title bar
// of the browser used to make the request. 'http://localhost' returns it as a query string parameter to a web server running on the client.
// See https://developers.google.com/accounts/docs/OAuth2InstalledApp#choosingredirecturi
NSString* const OAUTH_REDIRECT_URL = @"urn:ietf:wg:oauth:2.0:oob";
