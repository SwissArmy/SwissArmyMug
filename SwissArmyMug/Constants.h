
// OAuth constants

extern NSString* const OAUTH_SCOPE;
extern NSString* const OAUTH_AUTH_URL;
extern NSString* const OAUTH_TOKEN_URL;
extern NSString* const OAUTH_REDIRECT_URL;

// Sensitive constants

extern NSString* const OAUTH_CLIENT_ID;
extern NSString* const OAUTH_SECRET;
extern NSString* const OAUTH_ACCOUNT_TYPE;

// Macros

#ifdef DEBUG
#define L(fmt, ...) fprintf(stderr,"%s [Line %d] %s\n", __FUNCTION__, __LINE__, [[NSString stringWithFormat:fmt, ##__VA_ARGS__] UTF8String])
#define LS(fmt, ...) fprintf(stderr,"%s\n", [[NSString stringWithFormat:fmt, ##__VA_ARGS__] UTF8String])
#define RMAssert(A, B, ...) NSAssert(A, B, ##__VA_ARGS__)
#else
#define L(...)
#define LS(...)
#define RMAssert(A,B,...)
#endif