
import Foundation
import AppKit

class FolderViewManager: NSObject, NSOutlineViewDataSource, NSOutlineViewDelegate {

    var keys: NSArray

    override init() {
        var gpg = TPCocoaGPG(gpgPath: "/usr/local/MacGPG2/bin/gpg2", andHome: "~/.gnupg")
        keys = gpg.listPublicKeys()
    }

    // NSOutlineViewDelegate

    // NSOutlineViewDataSource

    func outlineView(outlineView: NSOutlineView, child index: Int, ofItem item: AnyObject?) -> AnyObject {
        return keys[index]
    }

    func outlineView(outlineView: NSOutlineView, numberOfChildrenOfItem item: AnyObject?) -> Int {
        return keys.count
    }
}