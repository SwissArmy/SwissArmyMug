//
//  main.m
//  SwissArmyMug
//
//  Created by Joshua Stewart on 3/12/13.
//  Copyright (c) 2013 Joshua Stewart. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
