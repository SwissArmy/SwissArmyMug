
import Foundation
import AppKit

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    var db: FMDatabase?

    override init() {
        super.init()
        db = nil
    }

    func applicationDidFinishLaunching(notification: NSNotification) {
        db = FMDatabase(path: "\(self.applicationFilesDirectory().path)/mail.db")
        if (!db!.open()) {
            // TODO: fail
            db = nil
        }
    }

    func applicationShouldTerminate(sender: NSApplication) -> NSApplicationTerminateReply {
        db!.close()
        return NSApplicationTerminateReply.TerminateNow
    }

    func applicationFilesDirectory() -> NSURL {
        return NSFileManager
            .defaultManager()
            // TODO: catch errors from this
            .URLForDirectory(.ApplicationSupportDirectory, inDomain: .UserDomainMask, appropriateForURL: nil, create: true, error: nil)!
            .URLByAppendingPathComponent("com.ExtropicStudios.SwissArmyMug")
    }
}