
#import <TPCocoaGPG.h>
#import <MailCore.h>

#import "ESOAuthWindowController.h"

@implementation ESOAuthWindowController

- (void)windowDidLoad {
    [super windowDidLoad];
    _folderView.dataSource = folderViewManager;
    _folderView.delegate = folderViewManager;
    _mailView.dataSource = mailViewManager;
    _mailView.delegate = mailViewManager;

    MCOIMAPSession* session = [[MCOIMAPSession alloc] init];
    [session setHostname:@"imap.gmail.com"];
    [session setPort:993];
    [session setUsername:@"ADDRESS@gmail.com"];
    [session setPassword:@"123456"];
    [session setConnectionType:MCOConnectionTypeTLS];

    MCOIMAPMessagesRequestKind requestKind = MCOIMAPMessagesRequestKindHeaders;
    NSString *folder = @"INBOX";
    MCOIndexSet *uids = [MCOIndexSet indexSetWithRange:MCORangeMake(1, UINT64_MAX)];

    MCOIMAPFetchMessagesOperation *fetchOperation = [session fetchMessagesByUIDOperationWithFolder:folder requestKind:requestKind uids:uids];

    [fetchOperation start:^(NSError * error, NSArray * fetchedMessages, MCOIndexSet * vanishedMessages) {
        //We've finished downloading the messages!

        //Let's check if there was an error:
        if (error) {
            NSLog(@"Error downloading message headers:%@", error);
        }

        //And, let's print out the messages...
        NSLog(@"The post man delivereth:%@", fetchedMessages);
    }];

    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
}

#pragma mark - WebResourceLoadDelegate

- (NSURLRequest*) webView:(WebView *)webView resource:(id)identifier willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)redirectResponse fromDataSource:(WebDataSource *)dataSource {
    LS(@"%@", request.URL);
    return request;
}

@end
