
#import <Cocoa/Cocoa.h>
#import <WebKit/WebKit.h>
#import "SwissArmyMug-Swift.h"

@interface ESOAuthWindowController : NSWindowController {
    FolderViewManager* folderViewManager;
    MailViewManager* mailViewManager;
}

@property (weak) IBOutlet NSOutlineView *folderView;
@property (weak) IBOutlet NSOutlineView *mailView;
@property (weak) IBOutlet WebView *webView;

@end
